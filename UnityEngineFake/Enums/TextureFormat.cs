using System;

namespace UnityEngine
{
	public enum TextureFormat
	{
		Alpha8 = 1,
		ARGB4444,
		RGB24,
		RGBA32,
		ARGB32,
		RGB565 = 7,
		DXT1 = 10,
		DXT5 = 12,
		PVRTC_2BPP_RGB = 30,
		PVRTC_2BPP_RGBA,
		PVRTC_4BPP_RGB,
		PVRTC_4BPP_RGBA,
		PVRTC_RGB2 = 30,
		PVRTC_RGBA2,
		PVRTC_RGB4,
		PVRTC_RGBA4,
		ETC_RGB4,
		ATC_RGB4,
		ATC_RGBA8,
		BGRA32,
		ATF_RGB_DXT1,
		ATF_RGBA_JPG,
		ATF_RGB_JPG
	}
}