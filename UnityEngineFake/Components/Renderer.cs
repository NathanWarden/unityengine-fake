using System;

namespace UnityEngine
{
	public class Renderer : Component
	{
		public Bounds bounds
		{
			get;
			private set;
		}

		public bool castShadows
		{
			get;
			set;
		}

		public bool enabled
		{
			get;
			set;
		}

		public bool isPartOfStaticBatch
		{
			get;
			private set;
		}

		public bool isVisible
		{
			get;
			private set;
		}

		public int lightmapIndex
		{
			get;
			set;
		}

		public Vector4 lightmapTilingOffset
		{
			get;
			set;
		}

		public Transform lightProbeAnchor
		{
			get;
			set;
		}

		public Matrix4x4 localToWorldMatrix
		{
			get;
			private set;
		}

		public Material material
		{
			get;
			set;
		}

		public Material[] materials
		{
			get;
			set;
		}

		public bool receiveShadows
		{
			get;
			set;
		}

		public Material sharedMaterial
		{
			get;
			set;
		}

		public Material[] sharedMaterials
		{
			get;
			set;
		}

		internal int staticBatchIndex
		{
			get;
			private set;
		}

		internal Transform staticBatchRootTransform
		{
			get;
			set;
		}

		public bool useLightProbes { get; set; }

		public Matrix4x4 worldToLocalMatrix
		{
			get;
			private set;
		}
	}
}