using System;

namespace UnityEngine
{
	public class SkinnedMeshRenderer : Renderer
	{
		//
		// Properties
		//

		internal Transform actualRootBone
		{
			get;
			private set;
		}

		public Transform[] bones
		{
			get;
			set;
		}

		public Bounds localBounds
		{
			get;
			set;
		}

		public SkinQuality quality
		{
			get;
			set;
		}

		public Transform rootBone
		{
			get;
			set;
		}

		public Mesh sharedMesh
		{
			get;
			set;
		}

		public bool updateWhenOffscreen
		{
			get;
			set;
		}

		//
		// Methods
		//

		public void BakeMesh (Mesh mesh)
		{
		}
	}
}