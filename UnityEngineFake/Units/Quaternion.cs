using System;

namespace UnityEngine
{
	public class Quaternion
	{
		public float x, y, z, w;

		public readonly static Quaternion identity = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
		public static Vector3 zero = new Vector3(0.0f, 0.0f, 0.0f);


		public Vector3 eulerAngles
		{
			get
			{
				return Vector3.zero;
			}
		}


		public Quaternion(float x, float y, float z, float w)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}


		public static Quaternion AngleAxis (float angle, Vector3 axis)
		{
			return Quaternion.identity;
		}


		public static Quaternion Euler(Vector3 eulerValue)
		{
			return Quaternion.identity;
		}


		public static Quaternion Euler(float x, float y, float z)
		{
			return Quaternion.identity;
		}


		public static Vector3 operator *(Quaternion a, Vector3 b)
		{
			Vector3 angle = a.eulerAngles;
			return new Vector3(angle.x * b.x, angle.y * b.y, angle.z * b.z);
		}


		public static Quaternion operator *(Quaternion a, Quaternion b)
		{
			return Quaternion.identity;
		}


		public static Quaternion FromToRotation(Vector3 vec1, Vector3 vec2)
		{
			return Quaternion.identity;
		}


		public static Quaternion Inverse(Quaternion q)
		{
			return new Quaternion(-q.x, -q.y, -q.z, -q.w);
		}


		public static Quaternion LookRotation(Vector3 vec)
		{
			return Quaternion.identity;
		}
	}
}