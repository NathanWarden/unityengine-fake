using System;

namespace UnityEngine
{
	public class Object
	{
		//
		// Fields
		//

		//private string m_UnityRuntimeErrorString;

		//private ReferenceData m_UnityRuntimeReferenceData;

		//
		// Properties
		//

		public HideFlags hideFlags
		{
			get;
			set;
		}

		public string name
		{
			get;
			set;
		}

		//
		// Static Methods
		//

		public static implicit operator bool (Object exists)
		{
			return !Object.CompareBaseObjects (exists, null);
		}


		public static bool operator == (Object x, Object y)
		{
			return Object.CompareBaseObjects (x, y);
		}


		public static bool operator != (Object x, Object y)
		{
			return !Object.CompareBaseObjects (x, y);
		}


		public override bool Equals (object o)
		{
			return Object.CompareBaseObjects (this, o as Object);
		}

		private static bool CompareBaseObjects (Object lhs, Object rhs)
		{
			return lhs == rhs;
		}


		public static void Destroy (Object obj)
		{
		}


		public override int GetHashCode ()
		{
			return this.GetInstanceID ();
		}


		public int GetInstanceID ()
		{
			return 0;
		}
	}
}