using System;
using System.Collections.Generic;


namespace UnityEngine
{
	public class GameObject : Object
	{
		Component[] components = new Component[0];


		public Component AddComponent (Type componentType)
		{
			Component newComponent = Activator.CreateInstance(componentType) as Component;
			List<Component> componentsList = new List<Component>(components);

			componentsList.Add(newComponent);
			components = componentsList.ToArray();

			return newComponent;
		}


		public T AddComponent<T> () where T : Component
		{
			return this.AddComponent (typeof(T)) as T;
		}
	}
}