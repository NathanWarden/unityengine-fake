using System;
using System.Runtime.CompilerServices;
namespace UnityEngine
{
	public class Texture : Object
	{
		//
		// Static Properties
		//

		public static AnisotropicFiltering anisotropicFiltering
		{
			get;
			set;
		}

		public static int masterTextureLimit
		{
			get;
			set;
		}

		//
		// Properties
		//

		public int anisoLevel
		{
			get;
			set;
		}

		public FilterMode filterMode
		{
			get;
			set;
		}

		public virtual int height
		{
			get
			{
				return Texture.Internal_GetHeight (this);
			}
			set
			{
				throw new Exception ("not implemented");
			}
		}

		public float mipMapBias
		{
			get;
			set;
		}

		public Vector2 texelSize
		{
			get
			{
				Vector2 result;
				Texture.Internal_GetTexelSize (this, out result);
				return result;
			}
		}

		public virtual int width
		{
			get
			{
				return Texture.Internal_GetWidth (this);
			}
			set
			{
				throw new Exception ("not implemented");
			}
		}

		public TextureWrapMode wrapMode
		{
			get;
			set;
		}

		//
		// Static Methods
		//

		private static int Internal_GetHeight (Texture mono) { return -1; }

		private static void Internal_GetTexelSize (Texture tex, out Vector2 output) { output = Vector2.zero; }

		private static int Internal_GetWidth (Texture mono) { return -1; }

		public static void SetGlobalAnisotropicFilteringLimits (int forcedMin, int globalMax) {}

		//
		// Methods
		//

		public int GetNativeTextureID () { return -1; }
	}
}