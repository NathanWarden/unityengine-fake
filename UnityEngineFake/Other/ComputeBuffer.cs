using System;
using System.Security;


namespace UnityEngine
{
	public sealed class ComputeBuffer : IDisposable
	{
		//
		// Fields
		//

		[NonSerialized]
		private IntPtr ptr;

		//
		// Constructors
		//

		~ComputeBuffer ()
		{
			this.Dispose (false);
		}

		public ComputeBuffer (int count, int stride, ComputeBufferType type)
		{
			this.ptr = IntPtr.Zero;
			ComputeBuffer.InitBuffer (this, count, stride, type);

			if ( this.ptr == IntPtr.Zero )
			{
			}
		}

		public ComputeBuffer (int count, int stride) : this (count, stride, ComputeBufferType.Default)
		{
		}

		//
		// Static Methods
		//

		public static void CopyCount (ComputeBuffer src, ComputeBuffer dst, int dstOffset) {}

		private static void DestroyBuffer (ComputeBuffer buf) {}

		private static void InitBuffer (ComputeBuffer buf, int count, int stride, ComputeBufferType type) {}

		//
		// Methods
		//

		public void Dispose ()
		{
			this.Dispose (true);
			GC.SuppressFinalize (this);
		}

		private void Dispose (bool disposing)
		{
			ComputeBuffer.DestroyBuffer (this);
			this.ptr = IntPtr.Zero;
		}

		private void InternalSetData (Array data, int elemSize) {}

		public void Release ()
		{
			this.Dispose ();
		}

		[SecuritySafeCritical]
		public void SetData (Array data) {}
	}
}
