using NUnit.Framework;


namespace UnityEngine
{
	[TestFixture]
	public class GameObjectTests
	{
		GameObject gameObject;


		[SetUp]
		public void HandleSetup()
		{
			gameObject = new GameObject();
		}


		[TearDown]
		public void HandleTeardown()
		{
			Object.Destroy(gameObject);
		}


		[Test]
		public void GameObjectShouldNotBeNull()
		{
			Assert.IsNotNull(gameObject);
		}
	}
}