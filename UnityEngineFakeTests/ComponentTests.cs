using NUnit.Framework;


namespace UnityEngine
{
	[TestFixture]
	public class ComponentTests
	{
		GameObject gameObject;
		Renderer renderer;


		[SetUp]
		public void HandleSetup()
		{
			gameObject = new GameObject();
			renderer = gameObject.AddComponent<Renderer>();
		}


		[TearDown]
		public void HandleTeardown()
		{
			Object.Destroy(gameObject);
		}


		[Test]
		public void RendererShouldNotBeNull()
		{
			Assert.IsNotNull(renderer);
		}
	}
}