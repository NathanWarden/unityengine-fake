using System;
using System.Runtime.CompilerServices;
namespace UnityEngine
{
	public sealed class Texture2D : Texture
	{
		//
		// Properties
		//

		public TextureFormat format
		{
			get;
			private set;
		}

		public int mipmapCount
		{
			get;
			private set;
		}

		//
		// Constructors
		//

		public Texture2D (int width, int height, TextureFormat format, bool mipmap, bool linear)
		{
			Texture2D.Internal_Create (this, width, height, format, mipmap, linear);
		}

		public Texture2D (int width, int height, TextureFormat format, bool mipmap)
		{
			Texture2D.Internal_Create (this, width, height, format, mipmap, false);
		}

		public Texture2D (int width, int height)
		{
			Texture2D.Internal_Create (this, width, height, TextureFormat.ARGB32, true, false);
		}

		//
		// Static Methods
		//

		private static void INTERNAL_CALL_Compress (Texture2D self, bool highQuality) {}

		private static void INTERNAL_CALL_ReadPixels (Texture2D self, ref Rect source, int destX, int destY, bool recalculateMipMaps) {}

		private static void INTERNAL_CALL_SetPixel (Texture2D self, int x, int y, ref Color color) {}

		private static void Internal_Create (Texture2D mono, int width, int height, TextureFormat format, bool mipmap, bool linear) {}

		//
		// Methods
		//

		public void Apply (bool updateMipmaps)
		{
			bool makeNoLongerReadable = false;
			this.Apply (updateMipmaps, makeNoLongerReadable);
		}

		public void Apply ()
		{
			bool makeNoLongerReadable = false;
			bool updateMipmaps = true;
			this.Apply (updateMipmaps, makeNoLongerReadable);
		}

		public void Apply (bool updateMipmaps, bool makeNoLongerReadable) {}

		public void Compress (bool highQuality)
		{
			Texture2D.INTERNAL_CALL_Compress (this, highQuality);
		}

		public byte[] EncodeToPNG () { return new byte[0]; }

		public Color GetPixel (int x, int y) { return new Color(0, 0, 0); }

		public Color GetPixelBilinear (float u, float v) { return new Color(0, 0, 0); }

		public Color[] GetPixels (int x, int y, int blockWidth, int blockHeight, int miplevel) { return new Color[0]; }

		public Color[] GetPixels (int x, int y, int blockWidth, int blockHeight)
		{
			int miplevel = 0;
			return this.GetPixels (x, y, blockWidth, blockHeight, miplevel);
		}

		public Color[] GetPixels (int miplevel)
		{
			int num = this.width >> miplevel;
			if (num < 1)
			{
				num = 1;
			}
			int num2 = this.height >> miplevel;
			if (num2 < 1)
			{
				num2 = 1;
			}
			return this.GetPixels (0, 0, num, num2, miplevel);
		}

		public Color[] GetPixels ()
		{
			int miplevel = 0;
			return this.GetPixels (miplevel);
		}

		public Color32[] GetPixels32 (int miplevel) { return new Color32[0]; }

		public Color32[] GetPixels32 ()
		{
			int miplevel = 0;
			return this.GetPixels32 (miplevel);
		}

		private bool Internal_ResizeWH (int width, int height) { return false; }

		public bool LoadImage (byte[] data) { return false; }

		public Rect[] PackTextures (Texture2D[] textures, int padding)
		{
			bool makeNoLongerReadable = false;
			int maximumAtlasSize = 2048;
			return this.PackTextures (textures, padding, maximumAtlasSize, makeNoLongerReadable);
		}

		public Rect[] PackTextures (Texture2D[] textures, int padding, int maximumAtlasSize)
		{
			bool makeNoLongerReadable = false;
			return this.PackTextures (textures, padding, maximumAtlasSize, makeNoLongerReadable);
		}

		public Rect[] PackTextures (Texture2D[] textures, int padding, int maximumAtlasSize, bool makeNoLongerReadable) { return new Rect[0]; }

		public void ReadPixels (Rect source, int destX, int destY, bool recalculateMipMaps)
		{
			Texture2D.INTERNAL_CALL_ReadPixels (this, ref source, destX, destY, recalculateMipMaps);
		}

		public void ReadPixels (Rect source, int destX, int destY)
		{
			bool recalculateMipMaps = true;
			Texture2D.INTERNAL_CALL_ReadPixels (this, ref source, destX, destY, recalculateMipMaps);
		}

		public bool Resize (int width, int height, TextureFormat format, bool hasMipMap) { return false; }

		public bool Resize (int width, int height)
		{
			return this.Internal_ResizeWH (width, height);
		}

		public void SetPixel (int x, int y, Color color)
		{
			Texture2D.INTERNAL_CALL_SetPixel (this, x, y, ref color);
		}

		public void SetPixels (Color[] colors)
		{
			int miplevel = 0;
			this.SetPixels (colors, miplevel);
		}

		public void SetPixels (int x, int y, int blockWidth, int blockHeight, Color[] colors)
		{
			int miplevel = 0;
			this.SetPixels (x, y, blockWidth, blockHeight, colors, miplevel);
		}

		public void SetPixels (int x, int y, int blockWidth, int blockHeight, Color[] colors, int miplevel) {}

		public void SetPixels (Color[] colors, int miplevel)
		{
			int num = this.width >> miplevel;
			if (num < 1)
			{
				num = 1;
			}
			int num2 = this.height >> miplevel;
			if (num2 < 1)
			{
				num2 = 1;
			}
			this.SetPixels (0, 0, num, num2, colors, miplevel);
		}

		public void SetPixels32 (Color32[] colors)
		{
			int miplevel = 0;
			this.SetPixels32 (colors, miplevel);
		}

		public void SetPixels32 (Color32[] colors, int miplevel) {}
	}
}
