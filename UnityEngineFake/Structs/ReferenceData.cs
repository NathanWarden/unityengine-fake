using System;
namespace UnityEngine
{
	internal struct ReferenceData
	{
		//
		// Fields
		//

		public IntPtr cachedPtr;

		public int instanceID;
	}
}