using System;
namespace UnityEngine
{
	public struct CombineInstance
	{
		//
		// Fields
		//

		private Matrix4x4 m_Transform;

		private int m_SubMeshIndex;

		private Mesh m_Mesh;

		//
		// Properties
		//

		public Mesh mesh
		{
			get
			{
				return this.m_Mesh;
			}
			set
			{
				this.m_Mesh = value;
			}
		}

		public int subMeshIndex
		{
			get
			{
				return this.m_SubMeshIndex;
			}
			set
			{
				this.m_SubMeshIndex = value;
			}
		}

		public Matrix4x4 transform
		{
			get
			{
				return this.m_Transform;
			}
			set
			{
				this.m_Transform = value;
			}
		}
	}
}
