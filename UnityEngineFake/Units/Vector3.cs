using System;

namespace UnityEngine
{
	public class Vector3
	{
		public float x, y, z;

		public static readonly Vector3 zero = new Vector3(0.0f, 0.0f, 0.0f);
		public static readonly Vector3 one = new Vector3(1.0f, 1.0f, 1.0f);

		public static readonly Vector3 forward = new Vector3(0.0f, 0.0f, 1.0f);
		public static readonly Vector3 up = new Vector3(0.0f, 1.0f, 0.0f);
		public static readonly Vector3 right = new Vector3(1.0f, 0.0f, 0.0f);


		public Vector3(float x, float y, float z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}


		public static Vector3 operator *(Vector3 a, Vector3 b)
		{
			return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
		}


		public static Vector3 operator + (Vector3 a, Vector3 b)
		{
			return new Vector3 (a.x + b.x, a.y + b.y, a.z + b.z);
		}
	}
}