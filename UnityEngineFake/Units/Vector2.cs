using System;

namespace UnityEngine
{
	public class Vector2
	{
		public float x, y;

		public readonly static Vector2 zero = new Vector2(0.0f, 0.0f);
		public readonly static Vector2 one = new Vector2(1.0f, 1.0f);

		public readonly static Vector2 up = new Vector2(0.0f, 1.0f);
		public readonly static Vector2 right = new Vector2(1.0f, 0.0f);


		public Vector2(float x, float y)
		{
			this.x = x;
			this.y = y;
		}
	}
}