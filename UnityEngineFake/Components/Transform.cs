using System.Collections;


namespace UnityEngine
{
	public class Transform : Component, IEnumerable
	{
		//
		// Properties
		//

		public int childCount
		{
			get;
			private set;
		}

		public Vector3 eulerAngles
		{
			get
			{
				return this.rotation.eulerAngles;
			}
			set
			{
				this.rotation = Quaternion.Euler (value);
			}
		}

		public Vector3 forward
		{
			get
			{
				return this.rotation * Vector3.forward;
			}
			set
			{
				this.rotation = Quaternion.LookRotation (value);
			}
		}

		public Vector3 localEulerAngles
		{
			get;
			set;
		}

		public Vector3 localPosition
		{
			get;
			set;
		}

		public Quaternion localRotation
		{
			get;
			set;
		}

		public Vector3 localScale
		{
			get;
			set;
		}

		public Matrix4x4 localToWorldMatrix
		{
			get;
			private set;
		}

		public Vector3 lossyScale
		{
			get;
			private set;
		}

		public Transform parent
		{
			get;
			set;
		}

		public Vector3 position
		{
			get;
			set;
		}

		public Vector3 right
		{
			get;
			set;
		}

		public Transform root
		{
			get;
			private set;
		}

		public Quaternion rotation
		{
			get;
			set;
		}

		public Vector3 up
		{
			get
			{
				return this.rotation * Vector3.up;
			}
			set
			{
				this.rotation = Quaternion.FromToRotation (Vector3.up, value);
			}
		}

		public Matrix4x4 worldToLocalMatrix
		{
			get;
			private set;
		}

		//
		// Methods
		//

		public void DetachChildren ()
		{
		}

		public Transform Find (string name)
		{
			return null;
		}


		public Transform FindChild (string name)
		{
			return this.Find (name);
		}

		public Transform GetChild (int index)
		{
			return null;
		}


		public int GetChildCount ()
		{
			return 0;
		}

		public IEnumerator GetEnumerator ()
		{
			return null;
		}

		public Vector3 InverseTransformDirection (float x, float y, float z)
		{
			return this.InverseTransformDirection (new Vector3 (x, y, z));
		}

		public Vector3 InverseTransformDirection (Vector3 direction)
		{
			return Vector3.zero;
		}

		public Vector3 InverseTransformPoint (Vector3 position)
		{
			return Vector3.zero;
		}

		public Vector3 InverseTransformPoint (float x, float y, float z)
		{
			return this.InverseTransformPoint (new Vector3 (x, y, z));
		}

		public bool IsChildOf (Transform parent)
		{
			return false;
		}

		public void LookAt (Transform target)
		{
			Vector3 up = Vector3.up;
			this.LookAt (target, up);
		}

		public void LookAt (Transform target, Vector3 worldUp)
		{
			if (target)
			{
				this.LookAt (target.position, worldUp);
			}
		}

		public void LookAt (Vector3 worldPosition)
		{
		}

		public void LookAt (Vector3 worldPosition, Vector3 worldUp)
		{
		}

		public void Rotate (float xAngle, float yAngle, float zAngle)
		{
			Space relativeTo = Space.Self;
			this.Rotate (xAngle, yAngle, zAngle, relativeTo);
		}

		public void Rotate (float xAngle, float yAngle, float zAngle, Space relativeTo)
		{
			this.Rotate (new Vector3 (xAngle, yAngle, zAngle), relativeTo);
		}

		public void Rotate (Vector3 eulerAngles)
		{
			Space relativeTo = Space.Self;
			this.Rotate (eulerAngles, relativeTo);
		}

		public void Rotate (Vector3 eulerAngles, Space relativeTo)
		{
			Quaternion rhs = Quaternion.Euler (eulerAngles.x, eulerAngles.y, eulerAngles.z);
			if (relativeTo == Space.Self)
			{
				this.localRotation *= rhs;
			}
			else
			{
				this.rotation *= Quaternion.Inverse (this.rotation) * rhs * this.rotation;
			}
		}

		public void Rotate (Vector3 axis, float angle)
		{
			Space relativeTo = Space.Self;
			this.Rotate (axis, angle, relativeTo);
		}

		public void Rotate (Vector3 axis, float angle, Space relativeTo)
		{
			if (relativeTo == Space.Self)
			{
				this.RotateAround (base.transform.TransformDirection (axis), angle * 0.0174532924f);
			}
			else
			{
				this.RotateAround (axis, angle * 0.0174532924f);
			}
		}

		public void RotateAround (Vector3 point, Vector3 axis, float angle)
		{
		}

		public void RotateAround (Vector3 axis, float angle)
		{
		}

		public void RotateAroundLocal (Vector3 axis, float angle)
		{
		}

		public Vector3 TransformDirection (float x, float y, float z)
		{
			return this.TransformDirection (new Vector3 (x, y, z));
		}

		public Vector3 TransformDirection (Vector3 direction)
		{
			return Vector3.zero;
		}

		public Vector3 TransformPoint (Vector3 position)
		{
			return Vector3.zero;
		}

		public Vector3 TransformPoint (float x, float y, float z)
		{
			return this.TransformPoint (new Vector3 (x, y, z));
		}

		public void Translate (Vector3 translation)
		{
			Space relativeTo = Space.Self;
			this.Translate (translation, relativeTo);
		}

		public void Translate (Vector3 translation, Space relativeTo)
		{
			if (relativeTo == Space.World)
			{
				this.position += translation;
			}
			else
			{
				this.position += this.TransformDirection (translation);
			}
		}

		public void Translate (Vector3 translation, Transform relativeTo)
		{
			if (relativeTo)
			{
				this.position += relativeTo.TransformDirection (translation);
			}
			else
			{
				this.position += translation;
			}
		}

		public void Translate (float x, float y, float z, Transform relativeTo)
		{
			this.Translate (new Vector3 (x, y, z), relativeTo);
		}

		public void Translate (float x, float y, float z)
		{
			Space relativeTo = Space.Self;
			this.Translate (x, y, z, relativeTo);
		}

		public void Translate (float x, float y, float z, Space relativeTo)
		{
			this.Translate (new Vector3 (x, y, z), relativeTo);
		}
	}
}