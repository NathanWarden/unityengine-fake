using System;

namespace UnityEngine
{
	public struct Color
	{
		private float[] colors;

		public float this[int index] { get { return colors[index]; } }
		public float r { get { return colors[0]; } set { colors[0] = value; } }
		public float g { get { return colors[1]; } set { colors[1] = value; } }
		public float b { get { return colors[2]; } set { colors[2] = value; } }
		public float a { get { return colors[3]; } set { colors[3] = value; } }

		public static readonly Color red = new Color(1.0f, 0.0f, 0.0f);
		public static readonly Color green = new Color(0.0f, 1.0f, 0.0f);
		public static readonly Color blue = new Color(0.0f, 0.0f, 1.0f);
		public static readonly Color cyan = new Color(0.0f, 1.0f, 1.0f);
		public static readonly Color yellow = new Color(0.0f, 0.92f, 0.016f);
		public static readonly Color magenta = new Color(1.0f, 0.0f, 1.0f);
		public static readonly Color white = new Color(1.0f, 1.0f, 1.0f);
		public static readonly Color gray = new Color(0.5f, 0.5f, 0.5f);
		public static readonly Color grey = new Color(0.5f, 0.5f, 0.5f);
		public static readonly Color black = new Color(0.0f, 0.0f, 0.0f);
		public static readonly Color clear = new Color(0.0f, 0.0f, 0.0f, 0.0f);


		public Color(float r, float g, float b)
		{
			colors = new float[4];

			colors[0] = r;
			colors[1] = g;
			colors[2] = b;
			colors[3] = 1.0f;
		}


		public Color(float r, float g, float b, float a)
		{
			colors = new float[4];

			colors[0] = r;
			colors[1] = g;
			colors[2] = b;
			colors[3] = a;
		}


		public static Color operator *(Color a, float factor)
		{
			return new Color(a.colors[0] * factor, a.colors[1] * factor, a.colors[2] * factor, a.colors[3] * factor);
		}


		public static Color operator *(Color a, Color b)
		{
			return new Color(a.colors[0] * b.colors[0], a.colors[1] * b.colors[1], a.colors[2] * b.colors[2], a.colors[3] * b.colors[3]);
		}


		public static Color operator + (Color a, Color b)
		{
			return new Color (a.colors[0] + b.colors[0], a.colors[1] + b.colors[1], a.colors[2] + b.colors[2], a.colors[3] + b.colors[3]);
		}


		public static bool operator == (Color x, Color y)
		{
			return CompareColors (x, y);
		}


		public static bool operator != (Color x, Color y)
		{
			return !CompareColors (x, y);
		}


		public override bool Equals (object o)
		{
			return CompareColors (this, (Color)o);
		}


		private static bool CompareColors (Color lhs, Color rhs)
		{
			return lhs.r == rhs.r && lhs.g == rhs.g && lhs.b == rhs.b && lhs.a == rhs.a;
		}


		public override int GetHashCode ()
		{
			return base.GetHashCode();
		}


		public override string ToString ()
		{
			return "{ " + r.ToString("F2") + ", " + g.ToString("F2") + ", " + b.ToString("F2") + ", " + a.ToString("F2") + " }";
		}
	}
}