using System;

namespace UnityEngine
{
	public enum MeshTopology
	{
		Triangles,
		TriangleStrip,
		Quads,
		Lines,
		LineStrip,
		Points
	}
}