using System;

namespace UnityEngine
{
	public class Component : Object
	{
		public Animation animation
		{
			get;
			private set;
		}

		public AudioSource audio
		{
			get;
			private set;
		}

		public Camera camera
		{
			get;
			private set;
		}

		public Collider collider
		{
			get;
			private set;
		}

		public ConstantForce constantForce
		{
			get;
			private set;
		}

		public GameObject gameObject
		{
			get;
			private set;
		}


		public GUIText guiText
		{
			get;
			private set;
		}

		public GUITexture guiTexture
		{
			get;
			private set;
		}

		public HingeJoint hingeJoint
		{
			get;
			private set;
		}

		public Light light
		{
			get;
			private set;
		}

		public NetworkView networkView
		{
			get;
			private set;
		}

		public ParticleEmitter particleEmitter
		{
			get;
			private set;
		}

		public ParticleSystem particleSystem
		{
			get;
			private set;
		}

		public Renderer renderer
		{
			get;
			private set;
		}

		public Rigidbody rigidbody
		{
			get;
			private set;
		}

		public string tag
		{
			get;
			set;
		}

		public Transform transform
		{
			get;
			private set;
		}
	}
}