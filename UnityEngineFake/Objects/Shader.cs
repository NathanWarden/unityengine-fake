using System;


namespace UnityEngine
{
	public sealed class Shader : Object
	{
		//
		// Static Properties
		//

		public static int globalMaximumLOD
		{
			get;
			set;
		}

		//
		// Properties
		//

		public bool isSupported
		{
			get;
			private set;
		}

		public int maximumLOD
		{
			get;
			set;
		}

		public int renderQueue
		{
			get;
			private set;
		}

		//
		// Static Methods
		//

		public static void DisableKeyword (string keyword) {}

		public static void EnableKeyword (string keyword) {}

		public static Shader Find (string name)
		{
			Shader newShader = new Shader();
			newShader.name = name;
			return newShader;
		}

		internal static Shader FindBuiltin (string name) { return null; }

		private static void INTERNAL_CALL_SetGlobalColor (string propertyName, ref Color color) {}

		private static void INTERNAL_CALL_SetGlobalMatrix (string propertyName, ref Matrix4x4 mat) {}

		public static int PropertyToID (string name) { return -1; }

		public static void SetGlobalBuffer (string propertyName, ComputeBuffer buffer) {}

		public static void SetGlobalColor (string propertyName, Color color) {}

		public static void SetGlobalFloat (string propertyName, float value) {}

		public static void SetGlobalMatrix (string propertyName, Matrix4x4 mat) {}

		public static void SetGlobalTexGenMode (string propertyName, TexGenMode mode) {}

		public static void SetGlobalTexture (string propertyName, Texture tex) {}

		public static void SetGlobalTextureMatrixName (string propertyName, string matrixName) {}

		public static void SetGlobalVector (string propertyName, Vector4 vec) {}

		public static void WarmupAllShaders () {}
	}
}
