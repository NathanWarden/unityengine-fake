using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	public sealed class Mesh : Object
	{
		//
		// Properties
		//

		public Matrix4x4[] bindposes
		{
			get;
			set;
		}

		public BoneWeight[] boneWeights
		{
			get;
			set;
		}

		public Bounds bounds
		{
			get;
			set;
		}

		public Color[] colors
		{
			get;
			set;
		}

		public Color32[] colors32
		{
			get;
			set;
		}

		public Vector3[] normals
		{
			get;
			set;
		}

		public int subMeshCount
		{
			get;
			set;
		}

		public Vector4[] tangents
		{
			get;
			set;
		}

		public int[] triangles
		{
			get;
			set;
		}

		public Vector2[] uv
		{
			get;
			set;
		}

		public Vector2[] uv1
		{
			get;
			set;
		}

		public Vector2[] uv2
		{
			get;
			set;
		}

		public int vertexCount
		{
			get;
			private set;
		}

		public Vector3[] vertices
		{
			get;
			set;
		}

		//
		// Constructors
		//

		public Mesh ()
		{
			Mesh.Internal_Create (this);
		}

		//
		// Static Methods
		//

		private static void Internal_Create (Mesh mono)
		{
		}

		//
		// Methods
		//


		public void Clear ()
		{
			bool keepVertexLayout = true;
			this.Clear (keepVertexLayout);
		}

		public void Clear (bool keepVertexLayout) {}

		public void CombineMeshes (CombineInstance[] combine, bool mergeSubMeshes, bool useMatrices) {}

		public void CombineMeshes (CombineInstance[] combine, bool mergeSubMeshes)
		{
			bool useMatrices = true;
			this.CombineMeshes (combine, mergeSubMeshes, useMatrices);
		}

		public void CombineMeshes (CombineInstance[] combine)
		{
			bool useMatrices = true;
			bool mergeSubMeshes = true;
			this.CombineMeshes (combine, mergeSubMeshes, useMatrices);
		}

		public int[] GetIndices (int submesh) { return new int[0]; }

		public MeshTopology GetTopology (int submesh) { return MeshTopology.Triangles; }

		public int[] GetTriangles (int submesh) { return new int[0]; }

		public int[] GetTriangleStrip (int submesh) { return new int[0]; }

		private void INTERNAL_get_bounds (out Bounds value) { value = new Bounds(); }

		private void INTERNAL_set_bounds (ref Bounds value) {}

		public void MarkDynamic () {}

		public void Optimize () {}

		public void RecalculateBounds () {}

		public void RecalculateNormals () {}

		public void SetIndices (int[] indices, MeshTopology topology, int submesh) {}

		public void SetTriangles (int[] triangles, int submesh) {}

		internal void SetTrianglesDontRebuildCollisionTriangles (int[] triangles, bool isStrip, int submesh) {}

		public void SetTriangleStrip (int[] triangles, int submesh) {}
	}
}